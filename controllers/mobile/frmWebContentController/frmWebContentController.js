define({ 

 //Type your controller code here 
/* 
*This function is responsible for setting the appropiate requestURLConfig to the browser 
*widget on this form.
*This function is executed on the onNavigate event of this form.
*/
  
  onNavigateHandler : function (params) {
    kony.print("Entering into onNavigateHandler");
    this.view.browserForWebContent.requestURLConfig = params;
    kony.print("Exiting out of onNavigateHandler");
  }
  
 });