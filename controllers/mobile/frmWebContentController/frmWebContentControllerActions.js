define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for headerButtonLeft **/
    AS_Button_eb605d58313d4ee39f6296cc0fa5c5cb: function AS_Button_eb605d58313d4ee39f6296cc0fa5c5cb(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmLogin");
        ntf.navigate();
    },
    /** onNavigate defined for frmWebContent **/
    onNavigate: function AS_Form_fe6fbce5a8954aeeb60955755a5d58ec(eventobject) {
        var self = this;
        return self.onNavigateHandler.call(this, eventobject);
    }
});