define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTextChange defined for PassswordTextField **/
    AS_TextField_ff34385bc0074a52b46708f6bfc87880: function AS_TextField_ff34385bc0074a52b46708f6bfc87880(eventobject, changedtext) {
        var self = this;
        if (kony.theme.getCurrentTheme() != "default") {
            kony.theme.setCurrentTheme("default", function() {
                self.view.SignInButton.skin = "defBtnFocus";
            }, null);
        } else {
            (function() {
                self.view.SignInButton.skin = "defBtnFocus";
            })();
        }
    },
    /** onClick defined for SignInButton **/
    AS_Button_b5f2b7354c3a45aba255e9a1496ed8a6: function AS_Button_b5f2b7354c3a45aba255e9a1496ed8a6(eventobject) {
        var self = this;

        function INVOKE_SERVICE_je8e0d39a24d4f079af1d2b899dc65ab_Success(response) {
            kony.application.showLoadingScreen("CopyslFbox1", null, constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {});
            var ntf = new kony.mvc.Navigation("Form2");
            ntf.navigate();
            kony.application.dismissLoadingScreen();
        }
        function INVOKE_SERVICE_je8e0d39a24d4f079af1d2b899dc65ab_Failure(error) {}
        if (login_inputparam == undefined) {
            var login_inputparam = {};
        }
        login_inputparam["serviceID"] = "InfinityUserRepository$login";
        login_inputparam["operation"] = "login";
        InfinityUserRepository$login = mfidentityserviceinvoker("InfinityUserRepository", login_inputparam, INVOKE_SERVICE_je8e0d39a24d4f079af1d2b899dc65ab_Success, INVOKE_SERVICE_je8e0d39a24d4f079af1d2b899dc65ab_Failure);
    },
    /** onClick defined for btnFAQs **/
    AS_Button_e5a1f3c2443c47baa2099e40a70c3f0a: function AS_Button_e5a1f3c2443c47baa2099e40a70c3f0a(eventobject) {
        var self = this;
        return self.showWebContent.call(this, eventobject);
    },
    /** onClick defined for btnSupport **/
    AS_Button_a869f40b58aa44388e105ddbf4cb4dba: function AS_Button_a869f40b58aa44388e105ddbf4cb4dba(eventobject) {
        var self = this;
        return self.showWebContent.call(this, eventobject);
    },
    /** onClick defined for ButtonSquaredTripleRight **/
    AS_Button_g280c6f2bfad4e8c842931c9c66eeabd: function AS_Button_g280c6f2bfad4e8c842931c9c66eeabd(eventobject) {
        var self = this;

        function SHOW_ALERT_ab9e509da41846ca8dc1abc04abde142_True() {}
        function SHOW_ALERT_ab9e509da41846ca8dc1abc04abde142_Callback() {
            SHOW_ALERT_ab9e509da41846ca8dc1abc04abde142_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Error 404",
            "yesLabel": "ok",
            "message": "Error de Prueba",
            "alertHandler": SHOW_ALERT_ab9e509da41846ca8dc1abc04abde142_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    }
});