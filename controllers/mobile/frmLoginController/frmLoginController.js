define({ 

 //Type your controller code here 
/*
*This function is responsible for identifying the apropieate URL to be set to the browser 
*on the FormWebContent screen and to navigate to that screen
*/
  
  showWebContent : function(eventObject) {
    kony.print("Entering into showWebContent");
    if (eventObject.id === "btnFAQs")
      {
        urlConfig = { URL: webContent.faqs};
      }
    else if (eventObject.id === "btnSupport")
      {
        urlConfig = { URL: webContent.contactus};
      }
    var navigationObject = new kony.mvc.Navigation("frmWebContent");
    navigationObject.navigate(urlConfig);
    kony.print(" Exiting out of showWebContent");
  },
 });