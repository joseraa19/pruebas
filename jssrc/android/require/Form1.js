define("Form1", function() {
    return function(controller) {
        function addWidgetsForm1() {
            this.setDefaultUnit(kony.flex.DP);
            var UsernameTextField = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "UsernameTextField",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "57dp",
                "placeholder": "Username",
                "secureTextEntry": false,
                "skin": "defTextBoxNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "261dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "defTextBoxPlaceholder",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var PassswordTextField = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "PassswordTextField",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "57dp",
                "onTextChange": controller.AS_TextField_ff34385bc0074a52b46708f6bfc87880,
                "placeholder": "Password",
                "secureTextEntry": true,
                "skin": "defTextBoxNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "354dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "defTextBoxPlaceholder",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var SignInButton = new kony.ui.Button({
                "focusSkin": "CopydefBtnFocus0e98d36749dcb4c",
                "height": "50dp",
                "id": "SignInButton",
                "isVisible": true,
                "left": "57dp",
                "onClick": controller.AS_Button_b5f2b7354c3a45aba255e9a1496ed8a6,
                "skin": "CopydefBtnNormal0d3b21e2f5f2649",
                "text": "Sign In",
                "top": "467dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CantSignLabel = new kony.ui.Label({
                "id": "CantSignLabel",
                "isVisible": true,
                "left": "57dp",
                "skin": "CopydefLabel0e01ccd9e5d5b43",
                "text": "Can't Sign",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "406dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var headerContainer03 = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "56dp",
                "id": "headerContainer03",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "headerContainerSknMaster",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            headerContainer03.setDefaultUnit(kony.flex.DP);
            var headerTitleLabel = new kony.ui.Label({
                "height": "100%",
                "id": "headerTitleLabel",
                "isVisible": false,
                "left": "20dp",
                "right": "20dp",
                "skin": "headerTitleSkinMaster",
                "text": "Title",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var headerTitleLogo = new kony.ui.Image2({
                "centerY": "50%",
                "height": "29dp",
                "id": "headerTitleLogo",
                "isVisible": true,
                "left": "75dp",
                "skin": "slImage0b8da9551a1f442",
                "src": "kony_logowhitehoriz.png",
                "top": "0dp",
                "width": "65dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var headerIconRight = new kony.ui.Label({
                "centerY": "49%",
                "id": "headerIconRight",
                "isVisible": false,
                "right": 12,
                "skin": "headerCartIconSkin",
                "text": "",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var hamburgerIconContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "18dp",
                "id": "hamburgerIconContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0f54e6831b2364d",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {}, {});
            hamburgerIconContainer.setDefaultUnit(kony.flex.DP);
            var FlexContainer0f97e6e10d5ff40 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "id": "FlexContainer0f97e6e10d5ff40",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h819ee2043fa41",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {}, {});
            FlexContainer0f97e6e10d5ff40.setDefaultUnit(kony.flex.DP);
            FlexContainer0f97e6e10d5ff40.add();
            var aspdkaspdokaspdokaspdoakd = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "2dp",
                "id": "aspdkaspdokaspdokaspdoakd",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h819ee2043fa41",
                "top": "21dp",
                "width": "20dp",
                "zIndex": 1
            }, {}, {});
            aspdkaspdokaspdokaspdoakd.setDefaultUnit(kony.flex.DP);
            aspdkaspdokaspdokaspdoakd.add();
            var CopyFlexContainer0c1413b4b970f4a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "2dp",
                "id": "CopyFlexContainer0c1413b4b970f4a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h819ee2043fa41",
                "width": "20dp",
                "zIndex": 1
            }, {}, {});
            CopyFlexContainer0c1413b4b970f4a.setDefaultUnit(kony.flex.DP);
            CopyFlexContainer0c1413b4b970f4a.add();
            hamburgerIconContainer.add(FlexContainer0f97e6e10d5ff40, aspdkaspdokaspdokaspdoakd, CopyFlexContainer0c1413b4b970f4a);
            var headerButtonLeft = new kony.ui.Button({
                "focusSkin": "headerIconCartSkin",
                "height": "100%",
                "id": "headerButtonLeft",
                "isVisible": true,
                "left": "0dp",
                "skin": "headerButtonClearSkin",
                "text": "Back",
                "top": "0dp",
                "width": "41dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": false,
                "padding": [27, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var headerButtonRight = new kony.ui.Button({
                "focusSkin": "headerIconCartSkin",
                "height": "100%",
                "id": "headerButtonRight",
                "isVisible": false,
                "right": "0dp",
                "skin": "headerButtonClearSkin",
                "text": "Settings",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": false,
                "padding": [15, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            headerContainer03.add(headerTitleLabel, headerTitleLogo, headerIconRight, hamburgerIconContainer, headerButtonLeft, headerButtonRight);
            var SquaredButtonTriple = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "45dp",
                "id": "SquaredButtonTriple",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "746dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            SquaredButtonTriple.setDefaultUnit(kony.flex.DP);
            var ContainerTripleLeft = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "ContainerTripleLeft",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0dp",
                "width": "33.33%",
                "zIndex": 1
            }, {}, {});
            ContainerTripleLeft.setDefaultUnit(kony.flex.DP);
            var ButtonSquaredTripleLeft = new kony.ui.Button({
                "bottom": "5dp",
                "focusSkin": "ButtonSqrdSkinFocus",
                "id": "ButtonSquaredTripleLeft",
                "isVisible": true,
                "left": "10dp",
                "onClick": controller.AS_Button_e5a1f3c2443c47baa2099e40a70c3f0a,
                "right": "5dp",
                "skin": "ButtonSqrdSkinNormal",
                "text": "FAQ's",
                "top": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            ContainerTripleLeft.add(ButtonSquaredTripleLeft);
            var ContainerTripleMiddle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "ContainerTripleMiddle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.33%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0dp",
                "width": "33.33%",
                "zIndex": 1
            }, {}, {});
            ContainerTripleMiddle.setDefaultUnit(kony.flex.DP);
            var ButtonSquaredTripleMiddle = new kony.ui.Button({
                "bottom": "5dp",
                "focusSkin": "ButtonSqrdSkinFocus",
                "id": "ButtonSquaredTripleMiddle",
                "isVisible": true,
                "left": "5dp",
                "onClick": controller.AS_Button_a869f40b58aa44388e105ddbf4cb4dba,
                "right": "5dp",
                "skin": "ButtonSqrdSkinNormal",
                "text": "Support",
                "top": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            ContainerTripleMiddle.add(ButtonSquaredTripleMiddle);
            var ContainerTripleRight = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "ContainerTripleRight",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "CopyslFbox1",
                "top": "0dp",
                "width": "33.33%",
                "zIndex": 1
            }, {}, {});
            ContainerTripleRight.setDefaultUnit(kony.flex.DP);
            var ButtonSquaredTripleRight = new kony.ui.Button({
                "bottom": "5dp",
                "focusSkin": "ButtonSqrdSkinFocus",
                "id": "ButtonSquaredTripleRight",
                "isVisible": true,
                "left": "5dp",
                "onClick": controller.AS_Button_g280c6f2bfad4e8c842931c9c66eeabd,
                "right": "10dp",
                "skin": "ButtonSqrdSkinNormal",
                "text": "Button 3",
                "top": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            ContainerTripleRight.add(ButtonSquaredTripleRight);
            SquaredButtonTriple.add(ContainerTripleLeft, ContainerTripleMiddle, ContainerTripleRight);
            var NewAcountLabel = new kony.ui.Label({
                "id": "NewAcountLabel",
                "isVisible": true,
                "left": "57dp",
                "skin": "CopydefLabel0i71694ba990448",
                "text": "Open New Acount",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "536dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var Image0b3c27a092ba443 = new kony.ui.Image2({
                "height": "150dp",
                "id": "Image0b3c27a092ba443",
                "isVisible": true,
                "left": "132dp",
                "skin": "slImage",
                "src": "header_logo.png",
                "top": "75dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            this.add(UsernameTextField, PassswordTextField, SignInButton, CantSignLabel, headerContainer03, SquaredButtonTriple, NewAcountLabel, Image0b3c27a092ba443);
        };
        return [{
            "addWidgets": addWidgetsForm1,
            "enabledForIdleTimeout": false,
            "id": "Form1",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});