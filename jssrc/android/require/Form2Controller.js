define("userForm2Controller", {
    //Type your controller code here 
});
define("Form2ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTextChange defined for PassswordTextField **/
    AS_TextField_ff34385bc0074a52b46708f6bfc87880: function AS_TextField_ff34385bc0074a52b46708f6bfc87880(eventobject, changedtext) {
        var self = this;
        if (kony.theme.getCurrentTheme() != "default") {
            kony.theme.setCurrentTheme("default", function() {
                self.view.SignInButton.skin = "defBtnFocus";
            }, null);
        } else {
            (function() {
                self.view.SignInButton.skin = "defBtnFocus";
            })();
        }
    }
});
define("Form2Controller", ["userForm2Controller", "Form2ControllerActions"], function() {
    var controller = require("userForm2Controller");
    var controllerActions = ["Form2ControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
