define("userForm1Controller", {
    //Type your controller code here 
});
define("Form1ControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Button_e5a1f3c2443c47baa2099e40a70c3f0a: function AS_Button_e5a1f3c2443c47baa2099e40a70c3f0a(eventobject) {
        var self = this;
        kony.application.openURL('https://google.com');
    },
    AS_Button_a869f40b58aa44388e105ddbf4cb4dba: function AS_Button_a869f40b58aa44388e105ddbf4cb4dba(eventobject) {
        var self = this;
        kony.phone.openEmail(["josera.a19@gmail.com"], ["hola"], ["hola"], 'hola', "probando", false);
    },
    AS_Button_g280c6f2bfad4e8c842931c9c66eeabd: function AS_Button_g280c6f2bfad4e8c842931c9c66eeabd(eventobject) {
        var self = this;

        function SHOW_ALERT_ab9e509da41846ca8dc1abc04abde142_True() {}

        function SHOW_ALERT_ab9e509da41846ca8dc1abc04abde142_Callback() {
            SHOW_ALERT_ab9e509da41846ca8dc1abc04abde142_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Error 404",
            "yesLabel": "ok",
            "message": "Error de Prueba",
            "alertHandler": SHOW_ALERT_ab9e509da41846ca8dc1abc04abde142_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    },
    AS_TextField_ff34385bc0074a52b46708f6bfc87880: function AS_TextField_ff34385bc0074a52b46708f6bfc87880(eventobject, changedtext) {
        var self = this;
        if (kony.theme.getCurrentTheme() != "default") {
            kony.theme.setCurrentTheme("default", function() {
                self.view.SignInButton.skin = "defBtnFocus";
            }, null);
        } else {
            (function() {
                self.view.SignInButton.skin = "defBtnFocus";
            })();
        }
    },
    AS_Button_b5f2b7354c3a45aba255e9a1496ed8a6: function AS_Button_b5f2b7354c3a45aba255e9a1496ed8a6(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("Form2");
        ntf.navigate();
    }
});
define("Form1Controller", ["userForm1Controller", "Form1ControllerActions"], function() {
    var controller = require("userForm1Controller");
    var controllerActions = ["Form1ControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
